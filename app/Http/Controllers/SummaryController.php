<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Child;
use App\Schedule;
class SummaryController extends Controller
{
    public function GetChildInfos(Request $request)
    {
        $data = $request->validate([
            'code' => 'required|min:5|max:5',
            'date' => 'nullable'
        ]);
        
        $child = Child::get()->where('code', $data['code'])->first();
        $date;
        if ($request->filled('date')) {
            $month = substr($data['date'], -2);
            $year = substr($data['date'], 0,4);
            $summaries = Schedule::where('child_id', $child->id)->whereYear('current_date', '=', $year)->whereMonth('current_date', '=', $month)->get();
            $date = $data['date'];
        } else {
            $summaries = Schedule::where('child_id', $child->id)->whereMonth('current_date', '=', date('m'))->get();
            $date = date('Y-m');
        }
        
        // echo($summaries[0]->current_arrival);
        if ($child != null) {

            if (count($summaries) > 0) {
                return \View::make('summary')
                ->with(compact('child'))
                ->with(compact('summaries'))
                ->with("print", true)
                ->with("date", $date);
            } else {
                return \View::make('summary')
                ->with(compact('child'))
                ->with("print", false)
                ->with("date", $date);
            }
            
        } else {
            return redirect('/inconnu');
        }
    }
}
