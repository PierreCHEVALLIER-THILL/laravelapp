<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use \App\Child;
use \App\Schedule;

class ChildController extends Controller
{
    public function CheckUniqueChildCode($code)
    {
        $res = DB::table('children')->where('code', $code)->get();
        if (count($res) > 0 ){
            return false;   
        } 
        return true;
    }

    public function GenerateId($firstName, $lastName)
    {
        $res = "";
        $letters = strtoupper(substr($firstName, 0,1).substr($lastName, 0,1));
        $number = rand(00,999);
        $number < 10? $res = $letters."00".$number : $res = $letters.$number;
        $number < 100? $res = $letters."0".$number : $res = $letters.$number;
        if (!$this->CheckUniqueChildCode($res)) {
            generateId($firstName, $lastName);
        }
        return $res;
    }

    public function InsertChild(Request $request)
    {
        $data = $request->validate([
            'firstName' => 'required|min:2|max:255',
            'lastName' => 'required|max:255',
            'birthDate' => 'required',
            'arrival' => 'required',
            'departure' => 'required'
        ]);
        echo("InsertChild=> ".$data['birthDate']);
        $child = new Child;
        $child->code = $this->GenerateId($data['firstName'], $data['lastName']);
        $child->firstName = $data['firstName'];
        $child->lastName = $data['lastName'];
        $child->birthDate = $data['birthDate'];   
        $child->arrival = $data['arrival'];
        $child->departure = $data['departure'];
    
        $child->save();
        return redirect('/');
    }

    public function GetChildInfos(Request $request)
    {
        $data = $request->validate([
            'code' => 'required|min:5|max:5',
        ]);
        
        $child = Child::get()->where('code', $data['code'])->first();
        $currentTime = date("H:i:s");

        if ($child != null) {
            $schedule =  Schedule::get()->where('child_id', $child->id)->where('current_date', date('Y-m-d'))->first();
            if ($schedule == null) {
                return \View::make('childInformations')
                ->with(compact('child'))
                ->with("arrival", $currentTime)
                ->with("departure", null);
            } else if ($schedule != null && $schedule->current_departure == null) {
                return \View::make('childInformations')
                ->with(compact('child'))
                ->with("arrival", $schedule->current_arrival)
                ->with("departure", $currentTime);
            } else if($schedule != null && $schedule->current_departure != null) {
                return redirect('/complete');
            }
        } else {
            return redirect('/inconnu');
        }
    }
}