<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use App\Child;
use App\Schedule;

class ScheduleController extends Controller
{

    public function HandleScheduleCreation(Request $request, $data)
    {
        $currentChild = Child::find($data['id']);
        $schedule =  Schedule::get()->where('child_id', $currentChild->id)->where('current_date', date('Y-m-d'))->first();
        if ($schedule == null) {
            $schedule = new Schedule;
        }
        $schedule->current_arrival = $data['arrival'];
        $schedule->current_departure = $request->filled('departure') ? $data['departure'] : null;
        $schedule->current_date = new \DateTime('NOW', new \DateTimeZone("Europe/Paris"));
        $currentChild->schedules()->save($schedule);
    }

    public function InsertSchedule(Request $request)
    {
        $data = $request->validate([
            'id' => 'required',
            'arrival' => 'required',
            'departure' => 'nullable',
        ]);
        $this->HandleScheduleCreation($request, $data);

        return redirect('/');
    }
}
