<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use App\Schedule;

class Child extends Model
{
    protected $fillable = [
        'code',
        'firstName',
        'lastName',
        'birthDate',
        'arrival',
        'departure'
    ];


    public function schedules()
    {
        return $this->hasMany(Schedule::class);
    }

    public function GetMonths()
    {
        $datetime1 = new \DateTime('NOW',  new \DateTimeZone("Europe/Paris"));
        $datetime2 = \Carbon\Carbon::parse($this->birthDate);
        $interval = $datetime1->diff($datetime2);
        $nbyear= $interval->format('%y');
        $nbmonth= $interval->format('%m');
        $nbmonth+=$nbyear*12;
        return ($nbmonth);
    }
}
