<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Schedule extends Model
{
    protected $fillable = [
        'current_arrival',
        'current_departure',
        'current_date'
    ];

    public function child()
    {
        return $this->belongsTo(Child::class);
    }

    public function CalcSupp($child)
    {
        $var = strtotime($child->arrival) - strtotime($this->current_arrival);
        if ($var <= 0) {
            $var = 0;
        }
        $var2 = strtotime($this->current_departure) - strtotime($child->departure);
        if ($var2 <= 0) {
            $var2 = 0;
        }
        $extraTime = ($var + $var2) / 60;
        $res = intdiv($extraTime, 30);
        if (($extraTime % 30) > 0) {
            $res++;
        }
        $minutes = $res * 30;
        $concat = "+".intdiv($minutes, 60)."h".($minutes%60);

        return ($minutes ? $concat : null);
    }
}
