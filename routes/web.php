<?php

use Illuminate\Http\Request;

Route::get('/', function () {
    return view('welcome');
});

Route::get('/inconnu', function () {
    return view('errorBabyUnknown');
});

Route::get('/complete', function () {
    return view('errorAlreadyInsert');
});

Route::get('/souscription', function () {
    return view('subscription');
});
Route::post('/souscription', 'ChildController@InsertChild')->name('InsertChild');

Route::post('/enregistrement', 'ChildController@GetChildInfos')->name('GetChildInfos');


Route::get('/confirmation', function () {
    return view('childInformations');
});

Route::post('/confirmation', 'ScheduleController@InsertSchedule')->name('InsertSchedule');


Route::get('/recapitulatif-form', function () {
    return view('summaryForm');
});

Route::post('/recapitulatif', 'SummaryController@GetChildInfos')->name('GetSummaryInfos');

Auth::routes();

Route::get('/home', 'HomeController@index')->name('home');
