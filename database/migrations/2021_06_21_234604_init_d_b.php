<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class InitDB extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::dropIfExists('children');
        Schema::create('children', function (Blueprint $table) {
            $table->increments('id')->unique();
            $table->string('code');
            $table->string('firstName');
            $table->string('lastName');
            $table->date('birthDate');
            $table->time('arrival');
            $table->time('departure');
            $table->timestamps();
        });

        Schema::dropIfExists('schedules');
        Schema::create('schedules', function (Blueprint $table) {
            $table->increments('id')->unique();
            $table->integer('child_id')->unsigned();
            $table->date('current_date');
            $table->time('current_arrival');
            $table->time('current_departure')->nullable();
            $table->timestamps();
            $table->foreign('child_id')->references('id')->on('children')->onDelete('cascade')->onUpdate('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('children');
    }
}
