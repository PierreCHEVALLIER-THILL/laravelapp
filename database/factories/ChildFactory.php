<?php

/** @var \Illuminate\Database\Eloquent\Factory $factory */

use App\Child;
use App\Schedule;
use App\Http\Controllers\ChildController;
use Faker\Generator as Faker;

$factory->define(Child::class, function (Faker $faker) {
    $controller = new ChildController();
    $firstName = $faker->firstName();
    $lastName = $faker->lastName;
    $code = $controller->GenerateId($firstName, $lastName);
    return [
        'firstName' => $firstName,
        'lastName' => $lastName,
        'code' => $code,
        'birthDate' => $faker->dateTimeBetween($startDate = '-2 years', $endDate = 'now'),
        'arrival' => $faker->time($format = 'H:i:s', $max = 'now'),
        'departure' => $faker->time($format = 'H:i:s', $max = 'now'),
        'created_at' => date('Y-m-d H:i:s'),
        'updated_at' => date('Y-m-d H:i:s'),
    ];
});

// $factory->define(Schedule::class, function (Faker $faker) {
//     $users = App\Child::pluck('id')->toArray();
//     return [
//         'child_id' => $faker->randomElement($users),
//         'current_arrival' => $faker->time($format = 'H:i:s', $max = 'now'),
//         'current_departure' => $faker->time($format = 'H:i:s', $max = 'now'),
//         'current_date' => $faker->dateTimeBetween($startDate = '-2 years', $endDate = 'now'),
//         'created_at' => date('Y-m-d H:i:s'),
//         'updated_at' => date('Y-m-d H:i:s'),
//     ];
// });

// $factory->define(Schedule::class, function (Faker $faker) {
//     return [
//         'child_id' => function () {
//             return factory(Child::class)->create()->id;
//         },
//         'current_arrival' => $faker->time($format = 'H:i:s', $max = 'now'),
//         'current_departure' => $faker->time($format = 'H:i:s', $max = 'now'),
//         'current_date' => $faker->dateTimeBetween($startDate = '-2 years', $endDate = 'now'),
//         'created_at' => date('Y-m-d H:i:s'),
//         'updated_at' => date('Y-m-d H:i:s'),
//     ];
// });
