<?php

/** @var \Illuminate\Database\Eloquent\Factory $factory */

use App\Schedule;
use Faker\Generator as Faker;

$factory->define(Schedule::class, function (Faker $faker) {
    return [
        'child_id' => \App\Child::inRandomOrder()->first()->id,
        'current_arrival' => $faker->time($format = 'H:i:s', $max = 'now'),
        'current_departure' => $faker->time($format = 'H:i:s', $max = 'now'),
        'current_date' => $faker->dateTimeBetween($startDate = '-2 years', $endDate = 'now'),
        'created_at' => date('Y-m-d H:i:s'),
        'updated_at' => date('Y-m-d H:i:s'),
    ];
});
