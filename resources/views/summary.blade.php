<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
    <head>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1">

        <title>Laravel</title>

        <!-- Fonts -->
        <link href="https://fonts.googleapis.com/css?family=Nunito:200,600" rel="stylesheet">

        <!-- Styles -->
        <style>
            html, body {
                background-color: #fff;
                color: #636b6f;
                font-family: 'Nunito', sans-serif;
                font-weight: 200;
                height: 100vh;
                margin: 0;
            }

            .full-height {
                height: 100vh;
            }

            .flex-center {
                align-items: center;
                display: flex;
                justify-content: center;
            }

            .position-ref {
                position: relative;
            }

            .top-right {
                position: absolute;
                right: 10px;
                top: 18px;
            }

            .content {
                text-align: center;
            }

            .title {
                font-size: 84px;
            }

            .links > a {
                color: #636b6f;
                padding: 0 25px;
                font-size: 13px;
                font-weight: 600;
                letter-spacing: .1rem;
                text-decoration: none;
                text-transform: uppercase;
            }

            .m-b-md {
                margin-bottom: 30px;
            }
        </style>

        <script src = "https://ajax.googleapis.com/ajax/libs/jquery/2.1.3/jquery.min.js">
      </script>    
        <script>
            $(function () {
                $('input').on('click', function () {
            
                });
            });
      </script>
    </head>
    <body>
        <div class="flex-center position-ref full-height">

            <div class="content">
                <div class="title m-b-md">
                    Récapitulatif de {{ $child->firstName }}
                    {{ $child->lastName }}
                </div><br/>
                <div class="content">
                    BabyId:<b>{{  $child->code }}</b> <br/> 
                    <b>{{ $child->GetMonths() }} Mois </b> <br/>
                    Heure d'arrivée: <b>{{ $child->arrival }}</b>
                    Heure de départ: <b>{{ $child->departure }}</b>
                </div><br/><br/>
                <div>
                    <form action="{{ route('GetSummaryInfos') }}" method="post">
                        @csrf
                        @if ($errors->any())
                            <div class="alert alert-danger" role="alert">
                                Veuillez verifier les informations saisies
                            </div>
                        @endif
                        <div class="form-group">
                            <input type="hidden" id="code" name="code" value="{{ $child->code }}">
                            <label for="date">Veuillez selectionner le mois:</label><br/>
                            <input type="month" class="form-control @error('date') is-invalid @enderror" id="date" name="date" value="{{ $date }}">
                        </div>
                        <button type="submit" class="btn btn-primary">Rechercher</button>
                    </form>
                </div>
                <div class="links">
                </div><br/><br/>
                @if ($print == true)
                    <div>
                        <table class="table">
                            <caption>Présences du mois</caption>
                            <thead>
                                <tr>
                                <th scope="col">Date</th>
                                <th scope="col">Heure d'arrivé</th>
                                <th scope="col">Heure de départ</th>
                                <th scope="col">Suppléments</th>
                                </tr>
                            </thead>

                            <tbody>
                                @foreach ($summaries as $summary)
                                    <tr>
                                        <th scope="row">{{ $summary->current_date }}</th>
                                        <td>{{ \Carbon\Carbon::parse($summary->current_arrival)->format('H:i') }}</td>
                                        <td>{{ \Carbon\Carbon::parse($summary->current_departure)->format('H:i') }}</td>
                                        <td>{{ $summary->CalcSupp($child) }}</td>
                                    </tr>
                                @endforeach
                            </tbody>
                        </table>
                    </div>
                @else
                    <p>Pas d'enregistrement</p>
                @endif
                <a href="/">Page d'accueil</a>
            </div><br/><br/>
        </div>
    </body>
</html>
