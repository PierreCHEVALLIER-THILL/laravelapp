<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
    <head>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1">

        <title>Laravel</title>

        <!-- Fonts -->
        <link href="https://fonts.googleapis.com/css?family=Nunito:200,600" rel="stylesheet">

        <!-- Styles -->
        <style>
            html, body {
                background-color: #fff;
                color: #636b6f;
                font-family: 'Nunito', sans-serif;
                font-weight: 200;
                height: 100vh;
                margin: 0;
            }

            .full-height {
                height: 100vh;
            }

            .flex-center {
                align-items: center;
                display: flex;
                justify-content: center;
            }

            .position-ref {
                position: relative;
            }

            .top-right {
                position: absolute;
                right: 10px;
                top: 18px;
            }

            .content {
                text-align: center;
            }

            .title {
                font-size: 84px;
            }

            .links > a {
                color: #636b6f;
                padding: 0 25px;
                font-size: 13px;
                font-weight: 600;
                letter-spacing: .1rem;
                text-decoration: none;
                text-transform: uppercase;
            }

            .m-b-md {
                margin-bottom: 30px;
            }
        </style>
    </head>
    <body>
        <div class="flex-center position-ref full-height">
            <div class="content">
                <div class="title m-b-md">
                    {{ $child->firstName }}
                    {{ $child->lastName }}
                </div>
                <div class="links">
                BabyId:<b>{{  $child->code }}</b> <br/> 
                <b>{{ $child->GetMonths() }} Mois </b> <br/>
                </div><br/>
                <form action="{{ route('InsertSchedule') }}" method="post">
                    @csrf
                    @if ($errors->any())
                        <div class="alert alert-danger" role="alert">
                            Veuillez verifier les informations saisies
                        </div>
                    @endif
                    <div class="form-group">
                        <input type="hidden"  id="id" name="id" value="{{ $child->id }}">
                        <b>{{date('Y-m-d')}}</b><br/><br/>
                        
                        @if ($departure != null)
                            <div>
                                <label for="arrival">Heure d'arrivé:</label><br/>
                                <input type="time" class="form-control @error('arrival') is-invalid @enderror" id="arrival" readOnly="readOnly"  name="arrival" value="{{ $arrival }}"><br/><br/>
                                @error('arrival')
                                    <div class="invalid-feedback">{{ $message }}</div>
                                @enderror
                                <label for="departure">Heure de départ:</label><br/>
                                <input type="time" class="form-control @error('departure') is-invalid @enderror" id="departure" name="departure" value="{{ $departure }}"><br/><br/>
                                @error('departure')
                                    <div class="invalid-feedback">{{ $message }}</div>
                                @enderror
                            </div>
                        @else
                            <div>
                                <label for="arrival">Heure d'arrivé:</label><br/>
                                <input type="time" class="form-control @error('arrival') is-invalid @enderror" id="arrival"  name="arrival" value="{{ $arrival }}"><br/><br/>
                                @error('arrival')
                                    <div class="invalid-feedback">{{ $message }}</div>
                                @enderror
                            </div>
                        @endif
                    </div>
                    <button type="submit" class="btn btn-primary">Valider</button>
                    <a href="/">Annuler</a>
                </form>
            </div>
        </div>
    </body>
</html>
