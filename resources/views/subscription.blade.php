@extends('layouts.app')
@section('content')
    <div class="container">
        <div class="row">
            <h1>Inscription</h1>
        </div>
        <div class="row">
            <form action="{{ route('InsertChild') }}" method="post">
                @csrf
                @if ($errors->any())
                    <div class="alert alert-danger" role="alert">
                        Veuillez verifier les informations saisies
                    </div>
                @endif
                <div class="form-group">
                    <label for="firstName">Prénom:</label>
                    <input type="text" class="form-control @error('firstName') is-invalid @enderror" id="firstName" name="firstName" placeholder="Jules" value="{{ old('firstName') }}">
                    @error('firstName')
                        <div class="invalid-feedback">{{ $message }}</div>
                    @enderror
                </div>
                <div class="form-group">
                    <label for="lastName">Nom:</label>
                    <input type="text" class="form-control @error('lastName') is-invalid @enderror" id="lastName" name="lastName" placeholder="Dupont" value="{{ old('lastName') }}">
                    @error('lastName')
                        <div class="invalid-feedback">{{ $message }}</div>
                    @enderror
                </div>
                <div class="form-group">
                    <label for="birthDate">Date de naissance:</label>
                    <input type="date" class="form-control @error('birthDate') is-invalid @enderror" id="birthDate" name="birthDate" placeholder="1" value="{{ old('birthDate') }}">
                    @error('birthDate')
                        <div class="invalid-feedback">{{ $message }}</div>
                    @enderror
                </div>

                <div class="form-group">
                    <label for="arrival">Heure d'arrivée:</label>
                    <input type="time" class="form-control @error('arrival') is-invalid @enderror" id="arrival" name="arrival" placeholder="" value="{{ old('arrival') }}">
                    @error('birth')
                        <div class="invalid-feedback">{{ $message }}</div>
                    @enderror
                </div>
                <div class="form-group">
                    <label for="departure">Heure de départ:</label>
                    <input type="time" class="form-control @error('departure') is-invalid @enderror" id="departure" name="departure" placeholder="" value="{{ old('departure') }}">
                    @error('departure')
                        <div class="invalid-feedback">{{ $message }}</div>
                    @enderror
                </div>
                <button type="submit" class="btn btn-primary">Submit</button>
            </form>
        </div>
    </div>
@endsection