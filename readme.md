<p align="center"><img src="https://res.cloudinary.com/dtfbvvkyp/image/upload/v1566331377/laravel-logolockup-cmyk-red.svg" width="400"></p>

<p align="center">
<a href="https://travis-ci.org/laravel/framework"><img src="https://travis-ci.org/laravel/framework.svg" alt="Build Status"></a>
<a href="https://packagist.org/packages/laravel/framework"><img src="https://poser.pugx.org/laravel/framework/d/total.svg" alt="Total Downloads"></a>
<a href="https://packagist.org/packages/laravel/framework"><img src="https://poser.pugx.org/laravel/framework/v/stable.svg" alt="Latest Stable Version"></a>
<a href="https://packagist.org/packages/laravel/framework"><img src="https://poser.pugx.org/laravel/framework/license.svg" alt="License"></a>
</p>

## About Laravel

Laravel is a web application framework with expressive, elegant syntax. We believe development must be an enjoyable and creative experience to be truly fulfilling. Laravel takes the pain out of development by easing common tasks used in many web projects, such as:

- [Simple, fast routing engine](https://laravel.com/docs/routing).
- [Powerful dependency injection container](https://laravel.com/docs/container).
- Multiple back-ends for [session](https://laravel.com/docs/session) and [cache](https://laravel.com/docs/cache) storage.
- Expressive, intuitive [database ORM](https://laravel.com/docs/eloquent).
- Database agnostic [schema migrations](https://laravel.com/docs/migrations).
- [Robust background job processing](https://laravel.com/docs/queues).
- [Real-time event broadcasting](https://laravel.com/docs/broadcasting).

Laravel is accessible, powerful, and provides tools required for large, robust applications.

## Learning Laravel

Laravel has the most extensive and thorough [documentation](https://laravel.com/docs) and video tutorial library of all modern web application frameworks, making it a breeze to get started with the framework.

If you don't feel like reading, [Laracasts](https://laracasts.com) can help. Laracasts contains over 1500 video tutorials on a range of topics including Laravel, modern PHP, unit testing, and JavaScript. Boost your skills by digging into our comprehensive video library.

## Laravel Sponsors

We would like to extend our thanks to the following sponsors for funding Laravel development. If you are interested in becoming a sponsor, please visit the Laravel [Patreon page](https://patreon.com/taylorotwell).

- **[Vehikl](https://vehikl.com/)**
- **[Tighten Co.](https://tighten.co)**
- **[Kirschbaum Development Group](https://kirschbaumdevelopment.com)**
- **[64 Robots](https://64robots.com)**
- **[Cubet Techno Labs](https://cubettech.com)**
- **[Cyber-Duck](https://cyber-duck.co.uk)**
- **[British Software Development](https://www.britishsoftware.co)**
- **[Webdock, Fast VPS Hosting](https://www.webdock.io/en)**
- **[DevSquad](https://devsquad.com)**
- [UserInsights](https://userinsights.com)
- [Fragrantica](https://www.fragrantica.com)
- [SOFTonSOFA](https://softonsofa.com/)
- [User10](https://user10.com)
- [Soumettre.fr](https://soumettre.fr/)
- [CodeBrisk](https://codebrisk.com)
- [1Forge](https://1forge.com)
- [TECPRESSO](https://tecpresso.co.jp/)
- [Runtime Converter](http://runtimeconverter.com/)
- [WebL'Agence](https://weblagence.com/)
- [Invoice Ninja](https://www.invoiceninja.com)
- [iMi digital](https://www.imi-digital.de/)
- [Earthlink](https://www.earthlink.ro/)
- [Steadfast Collective](https://steadfastcollective.com/)
- [We Are The Robots Inc.](https://watr.mx/)
- [Understand.io](https://www.understand.io/)
- [Abdel Elrafa](https://abdelelrafa.com)
- [Hyper Host](https://hyper.host)

## Contributing

Thank you for considering contributing to the Laravel framework! The contribution guide can be found in the [Laravel documentation](https://laravel.com/docs/contributions).

## Security Vulnerabilities

If you discover a security vulnerability within Laravel, please send an e-mail to Taylor Otwell via [taylor@laravel.com](mailto:taylor@laravel.com). All security vulnerabilities will be promptly addressed.

## License

The Laravel framework is open-source software licensed under the [MIT license](https://opensource.org/licenses/MIT).



# Linux
## How start the project

Make sure you have:
- PHP >= 7
- Compiler
- Mysql >= 8
- Laravel 6


## Laravel

Place all folders/files inside the /var/www/html/ apache folder. 
Start your apach server. 
Go on your browser and try to accès to http/localhost. If the Apach index page is displaying, your server is running.
Then, try to accès to /laravel/public/
If you can't accès to this folder. Make sure your avec the reading accès to this folder wither your user profile:
- sudo chown -R www-data:www-data /var/www/html/laravelapp
- sudo chmod -R 775 /var/www/html/laravelapp/storage   
And add this environement variable (in your .bashrc/.zshrc):
- export COMPOSER=756890a4488ce9024fc62c56153228907f1545c228516cbf63f885e036d37e9a59d27d63f46af1d4d07ee0f76181c7d3



## MySQL Database

### Init

Run the following command to create mysql container and volume (-d = async).
docker-compose up -d

If you want to use an database manager, like DBeaver follow this instructions:

- Add new connection, and make sure to choose MySQL+8
- Insert DB name, user and pwd
- Edit driver setting and add to rules:
- - allowPublicKeyRetrieval true
- - useSSL false

Now you can manage your DataBase

### Migration


#### Check current migration
To do a migration use this cmd:
- php artisan migrate


#### Create table with migration
To create an table through migration use:
- php artisan make:migration create_links_table --create=links
Open the migration file created in database/migrations, update struct of the futur table.
Save migration file
Run the previous command:
- php artisan migrate

To apply the shema:
- php artisan migrate:fresh

#### Generate Model

To generate Model use:
- php artisan make:model --factory Link
To genereate Model with all other class Controller/Seed/Factory use:
- php artisan make:model Link --all
That create a new model in /app and a file factory in /database/factories

#### Generate Seeder

To generate Seeder use:
- php artisan make:seeder LinksTableSeeder
That create seed file in /database/seeds

#### Run Seeder

To launch seed script run this:
- php artisan db:seed

That lauch all seeds process. You can specify which launch
- php artisan migrate:fresh --seed

This command check delete all tables generate again the migration and run seed script

If the seed don't run correctly use this following cmd:
- ./composer.phar dump-autoload  

#### Create Controller

To create a controller:
- php artisan make:controller ChildController

### Tinker using

To use tinker:
- php artisan tinker
Open a tinker cmd
To ty any methode with params:
- app()->call('App\Http\Controllers\ChildController@checkUniqueChildCode', ["code" => "PiC69"]);


### Router cache

If you to reset the router cache:
- sudo php artisan route:cache